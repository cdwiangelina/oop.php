<?php

require "animal.php";
require "frog.php";
require "ape.php";

$sheep = new animal("shaun");


// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 2
// echo $sheep->cold_blooded // false

echo $sheep->get_name() . "<br>";
echo $sheep->get_legs(). "<br>";
echo $sheep->get_coldBlooded(). "<br>";

// index.php
$sungokong = new ape("kera sakti");
$sungokong->yell();// "Auooo"


$kodok = new frog("buduk");
$kodok->jump() ; // "hop hop"



// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>