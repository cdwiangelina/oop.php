<?php
    
    require_once 'animal.php';

    class frog extends animal{

        public function __construct($nama){
            $this->name = $nama;
            $this->legs = 4;
            $this->cold_blooded = "false";
        }

        public function jump(){
            echo "hop hop <br>";
        }

    }
?>