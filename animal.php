<?php

    class animal{
        public $name;
        public $legs;
        public $cold_blooded;

        public function __construct($nama){
            $this->name = $nama;
            $this->legs = 2;
            $this->cold_blooded = "false";

        }
  
        public function set_name($name1){
            $this->name = $name1;
        }

        public function set_legs($legs1){
            $this->legs= $legs1;
        }

        public function set_coldBlooded($cold_blooded){
            $this->cold_blooded = $cold_blooded;
        }

        public function get_name(){
            return $this->name;
        }

        public function get_legs(){
            return $this->legs;
        }

        public function get_coldBlooded(){
            return $this->cold_blooded;
        }

    }

?>